# Limehome Test

## Development
You need `Docker Compose` and `make` installed on your host.

You will also need a `.env` file with a valid `HERE_API_KEY` environment variable. You can use `.env.dist` provided here as a template, the rest of the variables are set for you.
```sh
$ cp .env.dist .env
```

You can run the application server and database with
```sh
$ make build up
```

and you can clean up the containers with
```sh
$ make clean
```

See `openapi.yaml` for the list of endpoints and expected inputs and outputs.

## Testing
Unfortunately automated tests are currently missing; I will try to add some if I have time. I tested manually with `curl`. Here's some sample requests to make your life easier:
```sh
$ curl http://localhost:3000/v1/hotels/import -X POST --header 'Content-Type: application/json' --data '{"lat": "48.130323", "lon": "11.576362" }'

$ curl http://localhost:3000/v1/hotels/search?lat=48.130323&lon=11.576362
$ curl http://localhost:3000/v1/hotels/search?lat=48.130323&lon=11.576362&radius=0

$ curl http://localhost:3000/v1/hotels/<_ID_FIELD_FROM_LAST_RESPONSE>/bookings -X POST --header 'Content-Type: application/json' --data '{"checkinDate":"2022-07-25", "checkoutDate": "2022-07-27", "amount": 150, "guestName": "Alex", "guestEmail":"email@test.de", "guestPhone":"+3030303030030"}'

$ curl http://localhost:3000/v1/hotels/<_ID_FIELD_FROM_LAST_RESPONSE>/bookings
```

## Corners Cut
- Since I had to skip automated tests to save time, I'd be happy to send you other assignments I have done that include tests.
- Bookings have no uniqueness constraints. This is intentional to make curl testing easier. You can make the exact same request to create a booking repeatedly.
- In production there would be more sanity checks, e.g. a checkout date should always be later than the checkin date. I made no such sanity checks except for some basic data type validation.
- The geospatial query to fetch nearby hotels from the DB relies on a legacy mechanism in Mongo and frankly, the base unit used does not seem in line with the docs. The current implementation showcases the functionality and the results are indeed monotonically correct: the lower the radius, the more the results are concentrated around the search point. The correct/modern way to implement geospatial search would be to use angular distance and then convert to flat distance units, but I thought that was too obfuscated for a hiring test.
- Logging, monitoring, and other production concerns have not been taken into account at all. A production build stage is included in the Dockerfile for illustration purposes.
- I did not set up rendering for the OpenAPI spec. I would recommend that you copy the contents into `https://editor.swagger.io/`for a nicer reading experience.
- I did not deploy a hosted version of the app because I do not have a free plan for a service that supports the project's workflow. I checked out GH Pages as per the assignment instructions, but it seems to only allow static websites. However, I hope that since the app is fully containerized it can already run easily enough on a local machine and therefore a hosted version is not necessary. If this is a pressing concern, I can look for another option.
