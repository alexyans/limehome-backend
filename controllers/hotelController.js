var HotelModel = require('../models/hotelModel.js');
const hereClient = require('../services/hereClient.js');

/**
 * hotelController.js
 *
 * @description :: Server-side logic for managing hotels.
 */
module.exports = {
    /**
     * hotelController.findBySearchCoordinates()
     */
    findBySearchCoordinates: function (req, res) {
        let {lat, lon, radius} = req.query

        HotelModel.find({
            coordinates: {
                $near: [
                    Number(lat),
                    Number(lon)
                ],
                $maxDistance: radius? Number(radius) : 1000
            }},
            ['name', 'coordinates'],
            function (err, hotels) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when getting hotels.',
                        error: err,
                        result: [],
                    });
                }

                return res.status(200).json({
                    message: '',
                    error: '',
                    result: hotels,
                });
            }
        );
    },

    /**
     * hotelController.import()
     */
    import: async function (req, res) {
        const {lat, lon} = req.body
        let hotelsFromSource

        try{
            const result = await hereClient.getHotelsByPointCoordinates(lat, lon)
            hotelsFromSource = result?.data?.items || []
        } catch(e) {
            return res.status(500).json({
                message: "Import failed: Failed to fetch from external source",
                error: e.message,
            })
        }
        
        // transform hotels to internal representation
        hotels = hotelsFromSource.map((item) => {
            return {
                name: item.title,
                coordinates: [item.position.lat, item.position.lng]
            }
        })

        // save to collection
        const result = await HotelModel.collection.insertMany(hotels, {ordered: false, rawResult: true})
        console.log(`Successfully imported ${result.insertedCount} hotels.`)
    
        if (result.insertedCount !== hotels.length) {
            return res.status(200).json({
                message: "Import partially succeeded: Some hotels failed to persist in DB",
                error: result.mongoose.validationErrors.join('\n'),
            })
        }

        return res.status(200).json({
            message: "Import succeeded.",
            error: '',
        })
    }
};
