const { query } = require('express');
var BookingModel = require('../models/bookingModel.js');

/**
 * bookingController.js
 *
 * @description :: Server-side logic for managing hotel bookings.
 */
module.exports = {
    /**
     * bookingController.listByHotelId()
     */
     listByHotelId: async function (req, res) {
        let bookings
        const hotelId = req.params.id
        try {
            bookings = await BookingModel.find({
                    hotelId: hotelId
                }, 
                ['_id', 'hotelId', 'checkinDate', 'checkoutDate', 'amount', 'guestName', 'guestEmail', 'guestPhone']
            ).exec()
        } catch(e) {
            return res.status(500).json({
                message: 'Error when getting bookings',
                error: e.message,
                result: [],
            });
        }

        return res.status(200).json({
            message: '',
            error: '',
            result: bookings,
        });
     },

    /**
     * bookingController.createByHotelId()
     */
    createByHotelId: async function (req, res) {
        // get number of bookings
        const result = await BookingModel.countDocuments({
            hotelId: req.params.id
        })

        // reached max number of bookings
        if (result >= 10) {
            return res.status(409).json({
                message: 'Booking creation failed',
                error: 'Hotel is booked to capacity'
            })
        }

        // validate and create booking
        const {
            checkinDate,
            checkoutDate,
            amount,
            guestName,
            guestEmail,
            guestPhone,
        } = req.body

        const booking = new BookingModel({
            hotelId: req.params.id,
            checkinDate: new Date(checkinDate),
            checkoutDate: new Date(checkoutDate),
            amount: Number(amount),
            guestName: guestName,
            guestEmail: guestEmail,
            guestPhone: guestPhone,
        })

        try {
            await booking.save()
        } catch(e) {
            return res.status(500).json({
                message: 'Booking creation failed',
                error: e.message
            })
        }

        return res.status(201).json({
            message: 'Booking created successfully',
            error: ''
        })
    }
};
