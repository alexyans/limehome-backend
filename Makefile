.PHONY: build up clean

build:
	docker-compose build

up:
	docker-compose up

clean:
	docker-compose down
