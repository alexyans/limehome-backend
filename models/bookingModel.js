var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var bookingSchema = new Schema({
	'hotelId' : {
		type: Schema.Types.ObjectId,
		ref: 'hotels',
		required: [true, "Hotel ID is required."]
	},
	'checkinDate' : {
		type: Date,
		required: [true, "Checkin date is required."]
	},
	'checkoutDate' : {
		type: Date,
		required: [true, "Checkout date is required."]
	},
	'amount' : {
		type: Number,
		required: [true, "Amount is required."]
	},
	'guestName' : {
		type: String,
		required: [true, "Guest name is required."]
	},
	'guestEmail' : {
		type: String,
		required: [true, "Guest email is required."]
	},
	'guestPhone' : {
		type: String,
		required: [true, "Guest phone is required."]
	},
});

module.exports = mongoose.model('bookings', bookingSchema);
