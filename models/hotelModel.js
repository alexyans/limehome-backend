var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var hotelSchema = new Schema({
	'name' : {
		type: String,
		required: [true, "Hotel name is required."]
	},
	'coordinates' : {
		type: [Number],
		index: '2d',
		required: [true, "Hotel coordinates are required."]
	}
});

module.exports = mongoose.model('hotels', hotelSchema);
