const axios = require('axios')
const BASE_URL = process.env.HERE_API_URL
const API_KEY = process.env.HERE_API_KEY

options = {
    method: 'GET',
    url: `${BASE_URL}/v1/discover`,
    params: { q: 'hotel', apiKey: API_KEY},
}

module.exports = {
    getHotelsByPointCoordinates: (lat, lon) => {
        console.log("in!")
        if (!lat || !lon) {
            throw new Error('No search coordinates were specified.')
        }

        options.params.at = `${lat},${lon}`
        console.log(options)
        return axios(options)
    },
}