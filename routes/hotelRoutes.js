var express = require('express');
var router = express.Router();
var hotelController = require('../controllers/hotelController.js');
var bookingController = require('../controllers/bookingController.js');

/* Get hotels around point coordinates from store */
router.get('/search', hotelController.findBySearchCoordinates);

/* Locate hotels around point coordinates and add them to store */
router.post('/import', hotelController.import);

/* List bookings by hotel ID */
router.get('/:id/bookings', bookingController.listByHotelId)

/* Create booking by hotel ID */
router.post('/:id/bookings', bookingController.createByHotelId);

module.exports = router;
