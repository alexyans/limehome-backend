var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');

var healthcheck = require('./routes/healthcheck');
var hotelsRouter = require('./routes/hotelRoutes');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', healthcheck);
app.use('/v1/hotels', hotelsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
});

module.exports = app;
